Date Time Formatter
===================

This module provides a field formatter for date field types which renders
values as an HTML `<time>` tag with a `datetime` attribute. This is helpful
when using Javascript to display a countdown, microformats, etc.

Usage
-----

1. Install this module as normal.

2. Manage display of a content type or other entity with a date field.

3. Select the "Time tag" formatter.

4. Change the formatter settings, to display the date values and format
   required.

5. Save the configuration.

Your date values should now be formatted something like this:

    <time datetime="2014-05-11T19:30:00+10:00">Sun May 11, 7:30pm</time>

